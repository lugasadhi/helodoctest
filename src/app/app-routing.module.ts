import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import {ListComponent} from './pages/list/list.component'
import {DetailComponent} from './pages/detail/detail.component'

const routes: Routes = [
  { path: 'list', component: ListComponent },
  { path: 'detail/:id', component: DetailComponent },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  }, 
];
const config: ExtraOptions = {
  useHash: true,
};
@NgModule({
  imports: [  RouterModule.forRoot(routes, config) ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
