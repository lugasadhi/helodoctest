import { Component, OnInit } from '@angular/core';
import {HttpRequestService} from './service/http-request.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'test';
  usersDetail:any;
  data:any;

  constructor(
    private _http:HttpRequestService
  ){
    
  }

  ngOnInit(){
    this.getDetailAccount();
  }

  async getDetailAccount(){
    const resp = await this._http.getDetailAcount().catch(error=>{console.log(error)});
    if(resp){
      this.data = resp;
    }
  }

}
