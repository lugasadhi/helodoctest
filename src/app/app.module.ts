import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareComponentsModule } from './pages/share-components/share-components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ListModule} from './pages/list/list.module'
import {DetailModule} from './pages/detail/detail.module'

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ShareComponentsModule,
    BrowserAnimationsModule,
    CommonModule,
    ListModule,
    DetailModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
