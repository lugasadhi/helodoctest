import { Component, OnInit } from '@angular/core';
import {HttpRequestService} from '../../service/http-request.service'
import {ActivatedRoute} from '@angular/router'
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  issue:any;
  constructor(
    private _actRoute:ActivatedRoute,
    private _http:HttpRequestService

  ) { 
    this._actRoute.paramMap.subscribe((res:any)=>{
      console.log(res)
      this.getDetail(res.params.id)
    })
  }

  ngOnInit(): void {
  }

  async getDetail(id:string){
    const resp:any = await this._http.getDetail(id).catch(err=>{
      console.log(err)
    })
    if(resp){
      console.log(resp)
      this.issue = resp;
    }
  }

}
