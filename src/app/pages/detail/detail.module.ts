import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './detail.component';
import { RouterModule  } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [DetailComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule
  ]
})
export class DetailModule { }
