import { Component, OnInit } from '@angular/core';
import {HttpRequestService} from '../../service/http-request.service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  filterText="";
  myVar:any;
  search="";
  per_page=10;
  page=1;
  showPage:any=[1,2,3]
  dataList:any;
  lst:any;
  constructor(
    private _http:HttpRequestService
  ){
    this.getList()
  }

  ngOnInit(): void {
  }

  async getList(){
    let param:any = {
      q : this.filterText?this.filterText:"repo:angular/angular/node+type:issue+state:open",
      per_page:this.per_page,
      page:this.page
    }
   
    const resp:any = await this._http.getList(param).catch(error=>{console.log(error)});
    if(resp){
      this.lst = resp;
      this.dataList = resp.items;
      this.setPage();
    }
  }


  applyFilter(event:any){
    clearTimeout(this.myVar);
    this.filterText = event.target.value.trim().toLowerCase();
    if(event.code == 'Enter'){
      this.getList()
    }else{
      this.myVar = setTimeout(() => { 
        this.getList()
      }, 700);
    }
  }

  nextPage(){
    this.page++;
    this.getList()
  }
  beforePage(){
    this.page = this.page <= 1?1:this.page-1;
    this.getList()
  }
  pageNumber(page:number){
    this.page = page;
    this.getList()
  }
  setPage(){
    this.showPage = undefined;
    this.showPage = []
    if(this.page < 3){
      this.showPage=[1,2,3,4,5]
    }else{
      this.showPage = [...this.showPage,this.page - 2];
      this.showPage = [...this.showPage,this.page -1];
      this.showPage = [...this.showPage,this.page ];
      this.showPage = [...this.showPage,this.page + 1];
      this.showPage = [...this.showPage,this.page + 2];
    }
  }
}

