import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(
    private http:HttpClient,
  ) { }

  baseUrl="https://api.github.com/";
  getHeader(){
    let headAuth = new HttpHeaders({
      'Content-Type':'application/json'
    })    
    return headAuth;
  }

  getDetailAcount(){
    let hdr = {
      headers : this.getHeader(),
    }
    return new Promise((resolve, reject)=>{
      this.http.get(this.baseUrl+"repos/angular/angular", hdr)
      .subscribe(
        (resp:any)=>{resolve(resp)},
        (err:any)=>{reject(err)}
      )
    });
  }

  getList(param:any){
    let hdr = {
      headers : this.getHeader(),
      params: param 
    }
    return new Promise((resolve, reject)=>{
      this.http.get(this.baseUrl+"search/issues", hdr)
      .subscribe(
        (resp:any)=>{resolve(resp)},
        (err:any)=>{reject(err)}
      )
    });
  }

  getDetail(id:string){
    let hdr = {
      headers : this.getHeader(),
    }
    return new Promise((resolve, reject)=>{
      this.http.get(this.baseUrl+"repos/angular/angular/issues/"+id, hdr)
      .subscribe(
        (resp:any)=>{resolve(resp)},
        (err:any)=>{reject(err)}
      )
    });
  }

}
